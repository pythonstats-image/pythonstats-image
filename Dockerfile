FROM ipython/scipyserver

RUN pip3 install nltk
RUN pip3 install gensim
RUN pip3 install statsmodels
RUN pip3 install cvxopt

RUN apt-get install -y cmake libcgal-dev libboost-python-dev libboost-signals-dev

RUN mkdir /build
ADD /build-dionysus.sh /build-dionysus.sh
RUN chmod u+x /build-dionysus.sh
RUN /build-dionysus.sh

ENV PYTHONPATH /build/Dionysus/build/bindings/python:$PYTHONPATH

RUN pip3 install scikit-image
RUN pip3 install mahotas
RUN pip3 install theano

RUN apt-get install -y libopencv-dev libgoogle-glog-dev libgflags-dev libprotobuf-dev libleveldb-dev libsnappy-dev libhdf5-dev liblmdb-dev

ADD /caffe-rc.tar.gz /caffe-rc.tar.gz
ADD /caffe-Makefile.config /caffe-Makefile.config
ADD /build-caffe.sh /build-caffe.sh
RUN chmod u+x /build-caffe.sh
RUN /build-caffe.sh
