#!/bin/bash

cd /build
tar -xvf /caffe-rc.tar.gz -C /build
cd caffe-rc
mv /caffe-Makefile.config ./Makefile.config
make all
make test
make runtest
