#!/bin/bash

cd /build
curl 'http://hg.mrzv.org/Dionysus/archive/tip.tar.gz' > tip.tar.gz
tar -xvf tip.tar.gz
mv Dionysus* Dionysus
cd Dionysus
mkdir build
cd build
cmake -Wno-dev ..
make -j2
